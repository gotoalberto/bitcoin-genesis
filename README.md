# bitcoin-genesis

This is a tool to generate Genesis Blocks.

You can use it to generate the original Bitcoin Genesis Block or the Testnet Bitcoin Genesis Block in order to build a custom blockchain-in-a-box.

Also, you can build your custom Alt-Coin starting your own BlockChain from your Genesis Block.

## Starting bitcoin-genesis utility

Building the original Bitcoin Genesis Block

```
$ java -jar bitcoin-genesis-jar-with-dependencies.jar -p /tmp/
```

The blk00000.dat will be created in /tmp path.

## Building your own Alt-Coin from a new Genesis Block

```
$ java -jar bitcoin-genesis-jar-with-dependencies.jar /tmp/ -m your-string
```

A new Genesis Block builded from your-string will be created in /tmp path.
blk00000.dat can be used to start a new BlockChain.

Do not forget change [the string description](https://github.com/bitcoin/bitcoin/blob/master/src/chainparams.cpp#L124) on your Bitcoin Client