package com.bitwise.bitcoingenesis;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.math.BigInteger;

import org.bouncycastle.util.encoders.Hex;

import com.google.bitcoin.core.Block;
import com.google.bitcoin.core.NetworkParameters;
import com.google.bitcoin.core.Transaction;
import com.google.bitcoin.core.TransactionInput;
import com.google.bitcoin.core.TransactionOutput;
import com.google.bitcoin.core.Utils;
import com.google.bitcoin.params.MainNetParams;
import com.google.bitcoin.script.Script;
import com.google.bitcoin.script.ScriptOpCodes;

public class GenesisBlock {

    private static final byte MAGIC[] = new byte[] { 0x0b, 0x11, 0x09, 0x07, 0x1d, 0x01, 0x00, 0x00 };
    private static final String DIFFICULTY = "04ffff001d010445";
    private static final String SIGNED_SCRIPT = "04678afdb0fe5548271967f1a67130b7105cd6a828e03909a67962e0ea1f61deb649f6bc3f4cef38c4f35504e51ec112de5c384df7ba0b8d578a4c702b6bf11d5f";

    private final byte[] block;

    public GenesisBlock(String message) throws Exception {
        this.block = serializeBlock(createGenesis(message));
    }

    public void writeToFile(String outputFile) throws Exception {
        FileOutputStream fos = new FileOutputStream(outputFile);
        fos.write(this.block);
        fos.close();
    }

    private static final byte[] serializeBlock(Block block) throws Exception {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Method[] methods = Block.class.getDeclaredMethods();
        Method method = null;
        for (Method m : methods) {
            if (m.getName().equals("bitcoinSerializeToStream")) {
                method = m;
                method.setAccessible(true);
            }
        }
        outputStream.write(MAGIC);
        method.invoke(block, outputStream);
        return outputStream.toByteArray();
    }

    private static final Block createGenesis(String message) throws Exception {
        NetworkParameters networkParameters = MainNetParams.get();
        Block genesisBlock = createBlock(networkParameters);
        Transaction tx = new Transaction(networkParameters);
        try {
            String encodedMsg = stringToHex(message);
            byte[] bytes = Hex.decode(DIFFICULTY + encodedMsg);
            tx.addInput(new TransactionInput(networkParameters, tx, bytes));
            ByteArrayOutputStream scriptPubKeyBytes = new ByteArrayOutputStream();
            Script.writeBytes(scriptPubKeyBytes, Hex.decode(SIGNED_SCRIPT));
            scriptPubKeyBytes.write(ScriptOpCodes.OP_CHECKSIG);
            tx.addOutput(new TransactionOutput(networkParameters, tx, Utils.toNanoCoins(50, 0),
                    scriptPubKeyBytes.toByteArray()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        genesisBlock.addTransaction(tx);
        return genesisBlock;
    }

    @SuppressWarnings("unchecked")
    private static final Block createBlock(NetworkParameters networkParameters) throws Exception {
        Constructor<Block> constructor = (Constructor<Block>) Block.class.getDeclaredConstructors()[3];
        constructor.setAccessible(true);
        Block block = constructor.newInstance(networkParameters);
        return block;
    }

    private static final String stringToHex(String message) throws Exception {
        return String.format("%040x", new BigInteger(1, message.getBytes("UTF-8")));
    }
}
