package com.bitwise.bitcoingenesis;

import java.io.File;

public class Main {

    private static final String FILE_NAME = "blk00000.dat";

    public static void main(String... args) throws Exception {
        CommandLine cli = CommandLine.fromArgList(args);
        GenesisBlock block = new GenesisBlock(cli.message);
        block.writeToFile(cli.path + File.separator + FILE_NAME);
    }

}
