package com.bitwise.bitcoingenesis;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

public class CommandLine {
    public static final String DEFAULT_MESSAGE = "The Times 03/Jan/2009 Chancellor on brink of second bailout for banks";

    @Parameter(names = { "-m", "--message" }, description = "Message to include in Genesis Block: "
            + DEFAULT_MESSAGE)
    public String message = DEFAULT_MESSAGE;

    @Parameter(names = { "-p", "--path" }, description = "Path to save the blk00000.dat file, example: /tmp"
            + DEFAULT_MESSAGE, required = true)
    public String path;

    public static CommandLine fromArgList(String[] args) {
        CommandLine cli = new CommandLine();
        new JCommander(cli, args);
        return cli;
    }
}
